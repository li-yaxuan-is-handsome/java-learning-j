## 1 封装

### 1.1 定义：

它指的是将对象的状态信息隐藏在对象内部，不允许外部程序直接访问对象内部信息，而是通过该类所提供的方法来实现对内部信息的操作和访问。如果想要访问需要通过严格的接口。

就例如看电视，我们只需要知道如何使用遥控器即可，而电视的内部构造等我们是无法也不需要理解的，这就是封装。

### 1.2优点：

1.  将低**耦合性****(尽量暴露少量方法给外部使用)。
2.  隐藏信息，实现维护代码安全。
3.  实现**高内聚**(类的内部数据细节由自己完成，不允许外部干涉)。

### 1.3使用：

对于每个值都提供对外访问的公共方法 -->getter和setter方法（遵循驼峰式命名规则）。**快捷键：ALT+Insert**

```
//水果类
public class Fruits {
    protected double weights;
    public double prices;
    
    public double getWeights() {
        return weights;
    }
    public void setWeights(double weights) {
        this.weights = weights;
    }
    public void setPrices(double prices){
        this.prices = prices;
    public double getPrices() {
        return prices * this.weights;
    }
}
```

```
//苹果类
public class Apple extends Fruits {
    public static void main(String[] args) {
        Fruits apple = new Fruits();
        //apple.weights = 3.67 这是个错误调用方法
        apple.setWeights(3.67);
        System.out.println(apple.getWeights());
        System.out.println(apple.getPrices());

    }
}
```

上述例子中，水果类是是苹果的父类，水果类中，封装了水果重量（private访问控制符），所以我们只可以使用getter和setter方法调用水果重量，如果直接调用，编译器会出现错误。水果类中出现的**this关键字**,是用来区分成员变量和局部变量。

## 2 继承

### 2.1特点：

-   **继承的本质是对某一类的抽象从而更好形成对世界的建模。**
-   Java的继承具有单继承的特点，每个子类只有一个直接父类。
-   Java的继承通过extends关键字来实现，实现继承的类被称为子类，被继承的类被称为父类。
-   子类具有父类的所有方法，同时还可以自己增加一些新方法。

苹果类继承了水果类所以水果类中的所有方法苹果类可以直接调用 。现在水果就是父类，而苹果就是子类。

在Java中java.lang.Object类是所有类的父类，要么是其直接父类，要么是其间接父类。因此所有的Java对象都可调用java.lang.Object类所定义的实例方法。

### 2.2supr方法：

（1）super是一个关键字。

（2）super和this很相似。

#### 2.2.1this:

（1）this能出现在实例方法和[构造方法](https://so.csdn.net/so/search?q=%E6%9E%84%E9%80%A0%E6%96%B9%E6%B3%95&spm=1001.2101.3001.7020)中；

（2）this的语法是“this.”和“this()”；

（3）this不能出现在静态方法中；

（4）在区分局部变量和实例变量时不能省略。

```
 public void setWeights(double weights) {
        this.weights = weights;
    }
```

（5）this()只能出现在构造方法的第一行

#### 2.2.2supr：

前几点和this类似，但this指向的是当前对象中的方法或字段，而supr指向的是当前对象的父类的所有特征，简而言之：如果想在子类中访问父类的方法可以使用这个关键字。supr()只能出现在构造方法的第一行。

```
public class Person {
    protected String name = "samlyx";
}
```

```
public class Teacher extends Person{
        private String name = "lyx";
        public void test(String name){
            System.out.println(name);
            System.out.println(this.name);
            System.out.println(super.name);
        }

}
```

```
public class Test02 {
    public static void main(String[] args) {
        Teacher tt = new Teacher("LLL");
        tt.test();
    }
}
```

```
LLL//输出结果
lyx
samlyx
```

但是父类的私有无法继承。如果父类的构造器不是无参构造器，那么子类就无法写无参构造。子类如果必须要使用无参，那么必须在构造器中先用supr调用父类有参，再构造自己的无参构造器。

```
public class Fruits {
    protected double weights;
    public double prices;

    public Fruits(double prices) {
        this.prices = prices;
    }
    public double getWeights() {
        return weights;
    }
    public void setWeights(double weights) {
        this.weights = weights;
    }
    public double getPrices() {
        return prices * this.weights;
    }
}
public class Apple extends Fruits {
    public Apple(double prices) {
        super(prices);//父类有参构造
        System.out.println("子类有参构造器");
    }
    public static void main(String[] args) {
        Fruits apple = new Fruits(3.2);

        apple.setWeights(3.67);
        System.out.println(apple.getWeights());
        System.out.println(apple.getPrices());

    }
}
```

### 2.3方法重写：

子类扩展了父类，子类是一个特殊的父类。大部分时候，子类总是以父类为基础，额外增加新的Field和方法。但有一种情况例外：子类需要重写父类的方法。**重写为方法的重写，和属性字段无关**

例如鸟类都包含了飞翔方法，其中鸵鸟是一种特殊的鸟类，因此鸵鸟应该是鸟的子类，因此它也将从鸟类获得飞翔方法，但这个飞翔方法明显不适合鸵鸟，为此，鸵鸟需要重写鸟类的方法。快捷键：Alt+Insert

```
public class Bird
{
//Bird类的fly方法
public void fly()
{
   System.out.println("我在天空里自由自在地飞翔...");
     }
}
```

```
        public class Ostrich extends Bird
        {
            //重写Bird类的fly方法
            @Override//注解
            public void fly()
            {
                  System.out.println("我只能在地上奔跑...");
            }
            public static void main(String[] args)
            {
                  //创建Ostrich对象
                  Ostrich os=new Ostrich();
                  //执行Ostrich对象的fly方法，将输出"我只能在地上奔跑..."
                  os.fly();
            }
        }
```

可以说子类重写了父类的方法，也可以说子类覆盖了父类的方法。当子类覆盖了父类方法后，子类的对象将无法访问父类中被覆盖的方法，但可以在子类方法中调用父类中被覆盖的方法。如果需要在子类方法中调用父类中被覆盖的方法，则可以使用super关键字。

方法的重写要遵循“两同两小一大”规则

-   “两同”即方法名相同、形参列表相同；
-   “两小”指的是子类方法返回值类型应比父类方法返回值类型更小或相等，子类方法声明抛出的异常类应比父类方法声明抛出的异常类更小或相等；
-   “一大”指的是子类方法的访问权限应比父类方法的访问权限更大或相等。

## 3多态

同一个方法可以根据发送对象的不同而采取不同的方法， 一个对象的实际类型是确定的但是引用类型是不定的 。

Java引用变量有两个类型：一个是编译时类型，一个是运行时类型。编译时类型由声明该变量时使用的类型决定，运行时类型由实际赋给该变量的对象决定。如果编译时类型和运行时类型不一致，就可能出现所谓的多态。

```
public class Test01 {
    public static void main(String[] args) {
        Student s1 = new Student();
        Person s2 = new Student();//父类的引用指向子类的类型
        Object s3 = new Student();
}
```

后面的student就是实际类型，是由自己固定的，但是前面的就可以由三种甚至更多中类型。