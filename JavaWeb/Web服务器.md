# Web服务器

## 1.Tomcat

### 1.1 简介

Tomcat是apache基金会下的一个核心项目，是一个开源免费的web服务器，支持servlet/jsp少量Javaee规范。

> 官网

https://tomcat.apache.org/



### 1.2 Tomcat基本使用

> 基本使用

- 下载：官网下载-> https://tomcat.apache.org/download-90.cgi

- 安装：绿色版，直接解压安装。

- 卸载：直接删除目录即可。

- 启动：双击->bin\startup.bat

  - 如果控制台乱码：修改conf/logging.properties

    ```properties
    java.util.logging.consoleHander.encoding = GBK
    ```

- 关闭：

  - 直接❌关闭。
  - bin\shutdown.bat 关闭。
  - Ctrl+c 关闭。



> 常见问题

- 启动窗口一闪而过：检查JAVA_HOME环境变量是否正确配置。
- 端口号冲突：找到对应的程序，将其关闭。

 

> 部署项目

将项目放在webapps目录下面，即可部署完成。



## 2.Servlet

### 2.1 简介

Servlet 为创建基于 web 的应用程序提供了基于组件、独立于平台的方法。它是作为来自 Web 浏览器或其他 HTTP 客户端的请求和 HTTP 服务器上的数据库或应用程序之间的中间层。使用 Servlet，您可以收集来自网页表单的用户输入，呈现来自数据库或者其他源的记录，还可以动态创建网页。

> 第一个程序

1. 首先构建一个Maven项目，如果已经创建了其他项目，可以右击该Moudel——>`Add Framework Support`进行maven的相关配置即可。我们在此工程下再建立Moudel进行编写代码。

![](C:\Users\86135\Pictures\Screenshots\Java\web服务器\转换为maven.png)

2. 在父项目`pom.xml`文件中进行servlet的相关包配置。

```xml
<dependencies>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>5.9.2</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>4.0.1</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet.jsp</groupId>
            <artifactId>jsp-api</artifactId>
            <version>2.2.1-b03</version>
        </dependency>
    </dependencies>
```

子项目会直接继承父项目的配置！

3. 编写servlet程序

   1. 编写一个类
   2. 实现servlet接口：servlet接口sun公司有两个默认实现类 `HttpServlet` `GenericServlet`

   ```java
   package com.li.demo01;
   
   import javax.servlet.ServletException;
   import javax.servlet.http.HttpServlet;
   import javax.servlet.http.HttpServletRequest;
   import javax.servlet.http.HttpServletResponse;
   import java.io.IOException;
   import java.io.PrintWriter;
   
   /**
    * @author SamLi
    * @version 1.0
    * @data 2023/8/7 - 18:24
    */
   public class HelloServlet extends HttpServlet {
       @Override
       protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
           System.out.println("进入doGet方法！");
           PrintWriter writer = resp.getWriter();// 响应流
           writer.print("Hello,Servlet!");
       }
   
       @Override
       protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
           super.doPost(req, resp);
       }
   }
   ```



4. 编写servlet的映射

我们编写的Java程序，需要通过浏览器访问，而浏览器访问需要连接web服务器，所有我们需要在web中注册我们的servlet以及给浏览器一个访问它的路径。

```xml
<!--  注册servlet-->
  <servlet>
    <servlet-name>hello</servlet-name>
    <servlet-class>com.li.demo01.HelloServlet</servlet-class>
  </servlet>
<!--  servlet请求路径-->
  <servlet-mapping>
    <servlet-name>hello</servlet-name>
    <url-pattern>/hello</url-pattern>
  </servlet-mapping>
```

5. 配置tomcat

![](C:\Users\86135\Pictures\Screenshots\Java\web服务器\配置tomcat.png)

6. 启动测试！

### 2.2 Servlet生命周期

servlet**生命周期**可定义为从创建到销毁的过程：

1. servlet初始化之后调用init()方法
2. servlet调用service()方法来响应客户端请求
3. servlet销毁前调用destory()方法
4. 最后由jvm的垃圾回收机制处理回收

#### 2.2.1 init()

init ()方法被设计成只调用一次。它在第一次创建 Servlet 时被调用，在后续每次用户请求时不再调用。

init ()方法的定义如下：

```java
public void init() throws ServletException {
  // 初始化代码...
}
```

#### 2.2.2 service()

service() 方法是执行实际任务的主要方法。Servlet 容器（即 Web 服务器）调用 service() 方法来处理来自客户端（浏览器）的请求，并把格式化的响应写回给客户端。

当用户调用一个 Servlet 时，就会创建一个 Servlet 实例，每一个用户请求都会产生一个新的线程，service() 方法检查 HTTP 请求类型（GET、POST、PUT、DELETE 等），并在适当的时候调用 doGet、doPost、doPut，doDelete 等方法。

service()方法定义如下：

```java
public abstract void service(ServletRequest var1, ServletResponse var2) throws ServletException, IOException;

```

#### 2.2.3 doGet() / doPost()

GET 请求来自于一个 URL 的正常请求，或者来自于一个未指定 METHOD 的 HTML 表单，它由 doGet() 方法处理。

POST 请求来自于一个特别指定了 METHOD 为 POST 的 HTML 表单，它由 doPost() 方法处理。

```java
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
```

#### 2.2.4 destory()

destroy() 方法只会被调用一次，在 Servlet 生命周期结束时被调用。destroy() 方法可以让您的 Servlet 关闭数据库连接、停止后台线程、把 Cookie 列表或点击计数器写入到磁盘，并执行其他类似的清理活动。

```java
  public void destroy() {
    // 终止化代码...
  }
```

### 2.3 servlet表单数据

servlet在处理表单数据时，会根据数据不同使用不同方法自动解析：

- **getParameter()：**您可以调用 **req.getParameter() **方法来获取表单参数的值。
- **getParameterValues()：**如果参数出现一次以上，则调用该方法，并返回多个值，例如复选框。
- **getParameterNames()：**如果您想要得到当前请求中的所有参数的完整列表，则调用该方法。

> 使用Get请求

1. 下面是处理 Web 浏览器输入的 **HelloForm.java** Servlet 程序。我们将使用 **getParameter()** 方法。

```java
package com.li.demo01;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author SamLi
 * @version 1.0
 * @data 2023/8/8 - 11:49
 */
@SuppressWarnings("all")
@WebServlet("/HelloForm")
public class HelloForm extends HttpServlet {
    private static final long UID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");

        PrintWriter writer = resp.getWriter();
        String title = "使用 Get 方法获取表单数据";
        String name =new String(req.getParameter("name").getBytes("ISO-8859-1"),"UTF-8");
        String docType = "<!DOCTYPE html> \n";
        writer.println(docType +
                "<html>\n" +
                "<head><title>" + title + "</title></head>\n" +
                "<body bgcolor=\"#f0f0f0\">\n" +
                "<h1 align=\"center\">" + title + "</h1>\n" +
                "<ul>\n" +
                "  <li><b>站点名</b>："
                + req.getParameter("name") + "\n" +
                "  <li><b>网址</b>："
                + req.getParameter("url") + "\n" +
                "</ul>\n" +
                "</body></html>");
        System.out.println("进入doGet方法~~");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

}
```

值得注意的是：我们可以不用再web.xml里面配置servlet，只需要加上**@WebServlet注解**就可以修改该servlet的属性了。web.xml可以配置的servlet属性，在@WebServlet中都可以配置。

2. 随后我们使用HTML表单进行数据响应

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>猴子教程(li.demo01.com)</title>
</head>
<body>
<form action="HelloForm" method="get">
    站点名:<input type="text" name="name">
    <br>
    网址:<input type="text" name="url">
    <input type="submit" value="提交">
</form>
</body>
</html>
```

将该文件保存到以下路径中：

![](C:\Users\86135\Pictures\Screenshots\Java\web服务器\html路径.png)

3. 运行，在表单数据中填数据之后响应



### 2.4 HttpServletRequest对象

HttpServletRequest对象：主要任务是用来接受客户端发送的请求，例如请求的参数，请求头等等信息。service()方法的形参接受是HttpServletRequest接口实例化的对象。

#### 2.4.1 接受请求

> 常用方法



| 方法             | 功能                          |
| ---------------- | ----------------------------- |
| getRequsetURL()  | 获取客户端发出请求时完整的URL |
| getRequsetURI()  | 获取请求行中的资源名称        |
| getQueryString() | 获取请求行中的参数部分        |
| getMethod()      | 获取客户端请求方式            |
| getProtocol()    | 获取HTTP版本号                |
| getContextPath() | 获取webapp名字                |

代码演示：

```java
package com.li.demo02;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author SamLi
 * @version 1.0
 * @data 2023/8/9 - 17:16
 */
@WebServlet("/s01")
public class Servlet01 extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 获取请求时的完整URL路径（从http开始到？结束）
        StringBuffer requestURL = request.getRequestURL();
        System.out.println("请求时的完整URL路径 " + requestURL);
        // 获取请求时部分URL路径（从项目站点名开始到？结束）
        String requestURI = request.getRequestURI();
        System.out.println("请求时部分URL路径 " + requestURI);
        // 获取请求时字符串参数（？号后面的所有字符串）
        String queryString = request.getQueryString();
        System.out.println("请求时字符串参数 " + queryString);
        // 获取请求方式（Get/Post）
        String method = request.getMethod();
        System.out.println("请求方式 " + method);
        // 获取当前协议版本号
        String protocol = request.getProtocol();
        System.out.println("当前协议版本号 " + protocol);
        // 获取项目站点名（项目对外访问路径）
        String contextPath = request.getContextPath();
        System.out.println("项目站点名 " + contextPath);

        // 获取指定名称参数参数
        String username = request.getParameter("username");
        String passwd = request.getParameter("passwd");
        System.out.println("username:" + username + "  passwd:" + passwd);
        // 获取指定名称所有参数值，用于复选框传值
        String[] values = request.getParameterValues("hobby");

        if (values != null && values.length > 0) {
            for (String value:values) {
                System.out.println("hobby:" + value );
            }
        }

    }
}

```

#### 2.4.2 请求乱码

  由于request用来接受客户端参数，那必定有默认的语言编码，在解析过程中默认使用ISO-8859-1的编码方式（不支持中文），所以会出现乱码情况，这时需要设置request的编码方式，或者在收到乱码数据之后，通过相应的编码格式还原。

**post请求一般会出现乱码！tomcat 8以上版本get请求不会出现乱码。**

**方式一：（只针对post请求）**

``` java 
request.setCharacterEncoding("utf-8");
```

**方式二：（如果本身不乱码，但依然使用，则会出现乱码）**

```java
new String(req.getParameter("name").getBytes("ISO-8859-1"),"UTF-8");
```

#### 2.4.3 请求转发

请求转发是一种服务器行为，当客户端请求到达后，服务器进行转发，此时会将请求对象进行保存，地址栏中的URL地址不会变化，得到响应后，服务器再将响应发送给客户端，**从始至终只有一个请求**

```java
package com.li;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author SamLi
 * @version 1.0
 * @data 2023/8/10 - 16:33
 */
@WebServlet("/s03")
public class Servlet03 extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        System.out.println("Servlet03 姓名：" + name);

        // 请求转发
        request.getRequestDispatcher("s04").forward(request,response);
    }
}

```

```java
package com.li;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author SamLi
 * @version 1.0
 * @data 2023/8/10 - 16:33
 */
@WebServlet("/s04")
public class Servlet04 extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        System.out.println("Servlet04 姓名：" + name);

    }
}

```

```txt
运行Servlet03会得到两个结果！！！
```

### 2.5 HttpServletResponese对象

web服务器收到客户端http请求，向客户端输出数据，需要用到response对象。

#### 2.5.1 响应数据

收到客户端请求，可以通过HttpServletResponese对象直接响应，响应时需要获取输出流：

| 代码              | 功能                           |
| ----------------- | ------------------------------ |
| getWriter()       | 获取字符流                     |
| getOutputStream() | 获取字节流（可以响应一切数据） |

**注意两个不可同时使用**

```java
// 响应字符流
PrintWriter writer = response.getWriter();
writer.write("Hi,World!");
```

```java
// 响应字节流
ServletOutputStream outputStream = response.getOutputStream();
outputStream.write("Hi,World!".getBytes(StandardCharsets.UTF_8));
```



#### 2.5.2 响应乱码

> 响应字符流乱码

与请求数据乱码情况一样，响应中文时也会出现乱码。

**方式一：**

```java
response.setCharacterEncoding("utf-8");
```

此处与请求乱码有一点不一样，要保证数据正确显示，还需要指定客户端的解码形式,**保证发送端和接受端一致**

```java
response.setHeader("content-type", "text/html;charset=utf-8");
```

**方式二：**

同时设置客户端和服务端的编码格式

```java
response.setContentType("text/html;charset=utf-8");
```



> 响应字节流乱码

需要指定客户端和服务端编码方式一致：

```java
response.setHeader("content-type", "text/html;charset=utf-8");
```

或者使用上面的万能解决方式**（同时设置客户端和服务端的编码格式）**

#### 2.5.3 重定向

重定向是一种服务器指导客户端的行为。客户端发出第一个请求时，服务端接受处理响应，响应同时服务端会给客户端一个新地址，客户端接收到响应后，会立即给新地址发出第二个请求，服务器接收并响应，重定向完成！

<img src="C:\Users\86135\Pictures\Screenshots\Java\web服务器\servlet重定向.png" style="zoom: 150%;" />

**请求转发和重定向区别**：

| 请求转发                      | 重定向                          |
| ----------------------------- | ------------------------------- |
| 一次请求，request域中数据共享 | 两次请求，request域中数据不共享 |
| 服务端行为                    | 客户端行为                      |
| 地址栏不会发生变化            | 地址栏发生变化                  |

### 2.6 Cookie对象

cookie其实就是一些数据信息，类型为“**小型文本文件**”，存储于电脑上的文本文件中。

当我们打开一个网站时，如果这个网站我们曾经登录过，那么当我们再次打开网站时，发现就不需要再次登录了，而是直接进入了首页。这其实就是游览器保存了我们的cookie，里面记录了一些信息。

**一般情况下，cookie是以键值对进行表示的(key-value)**

#### 2.6.1 Cookie的创建发送

要想将cookie随响应发送到客户端，需要先添加到response对象中（response。addCookie(Cookie)）

```java
package com.li;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * @author SamLi
 * @version 1.0
 * @data 2023/8/12 - 10:27
 */
@WebServlet("/cookie01")
public class Cookie01 extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // cookie的创建(如果有中文，必须先进行编码，获取cookie时URLDecoder.decode()进行解码)
        String name = "姓名";
        String value = "张三";
        //URLEncoder.encode进行设置编码
        URLEncoder.encode(name, "utf-8");
        URLEncoder.encode(value, "utf-8");

        Cookie cookie = new Cookie(name, value);

        // 响应cookie
        response.addCookie(cookie);
    }
}

```

<img src="C:\Users\86135\Pictures\Screenshots\Java\web服务器\cookie1.png" style="zoom:150%;" />



#### 2.6.2 Cookie的获取

在服务端只提供一个getcookies方法用来获取客户端回传的所以cookie数组，如果需要获取单独的cookie，则需要先遍历，getname获取cookie的名称，getvalue获取cookie的值。

```java
package com.li;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author SamLi
 * @version 1.0
 * @data 2023/8/12 - 10:56
 */
@WebServlet("/cookie02")
public class Cookie02 extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 获取cookie数组
        Cookie[] cookies = request.getCookies();

        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies){
                String name = cookie.getName();
                String value = cookie.getValue();

                System.out.println("name->: " + name + "    value->: " + value);
            }
        }

    }
}

```



#### 2.6.3 Cookie设置到期时间

我们还需要关心cookie的到期时间。通过setmaxage来设置cookie的最大有效时间。

- 负整数；
  - 表示不存储该cookie
  - maxage默值为-1，表示该cookie只在浏览器里存活，一旦关闭浏览器，cookie将会消失。
- 正整数；
  - 表示存储该cookie的秒数
  - 当默认值大于0时，浏览器会把cookie存到硬盘上，计算关闭客户端电脑，cookie也会存活指定时间。
- 零；
  - 表示删除该cookie

```java
response.setMaxAge(cookie)
```



### 2.7 HttpSession对象

对于服务器而言，每一个连接到它的客户端都是一个session，HttpSession是由JavaWeb提供的，用来会话跟踪的类。session是服务器端对象，保存在服务器端；

```java
package com.li;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author SamLi
 * @version 1.0
 * @data 2023/8/13 - 10:35
 */
@WebServlet("/session01")
public class Session01 extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 获取session对象
        HttpSession session = request.getSession();
        // 获取session标识符
        System.out.println(session.getId());
        // 获取session创建时间
        System.out.println(session.getCreationTime());
        // 获取session最后一次访问时间
        System.out.println(session.getLastAccessedTime());
        // 判断session是否为新的
        System.out.println(session.isNew());
    }
}

```

**session域对象操作：**

1. 设置session域对象

   ```java
   session.setAttribute("**","***");
   ```

2. 获取指定名称的session域对象

   ```java
   request.getAttribute("**");
   ```

3. 移除指定域对象

   ```java
   session.removeAttribute("***");
   ```

   

#### 2.7.1 Session销毁

> 默认时间到期

当客户端第一次请求servlet并且操作session时，session对象生成，tomcat中session默认存活时间为30分钟，可在conf目录下的web.xml文件修改存活时长。

```xml
<session-config>
        <session-timeout>30</session-timeout>
</session-config>
```

>自己设定到期时间

我们可以在程序中自己设定session到期时间；

```java
package com.li;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author SamLi
 * @version 1.0
 * @data 2023/8/13 - 20:42
 */
@WebServlet("/session02")
public class Session02 extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            session = request.getSession(true);
        }
        session.setAttribute("name", "value");
//      设置session最大不活动时间(秒)
        session.setMaxInactiveInterval(30);
        // 查看session最大存活时间
        int times = session.getMaxInactiveInterval();
        System.out.println("session最大存活时间为->" + times);
        System.out.println(session.getId());
//      销毁session
        session.invalidate();
    }
}

```

### 2.8 ServletContext对象

每一个web有且仅有一个servletcontext对象。

该对象一是用来共享数据，二是保存了当前应用程序相关信息；

#### 2.8.1 ServletContext对象获取

```java
package com.li;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author SamLi
 * @version 1.0
 * @data 2023/8/14 - 9:19
 */
@WebServlet("/context01")
public class Context01 extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 通过request对象获取
        ServletContext servletContext1 = request.getServletContext();
        // 通过session对象获取
        ServletContext servletContext2 = request.getSession().getServletContext();
        // 通过ServletConfig对象获取
        ServletContext servletContext3 = getServletConfig().getServletContext();
        // 直接获取
        ServletContext servletContext4 = getServletContext();

        /*
        * 常用方法
        * 1.获取当前服务器版本信息
        * 2.获取项目真实路径
        */
        String serverInfo = servletContext1.getServerInfo();
        System.out.println("当前服务器版本信息为-> " + serverInfo);

        String realPath = servletContext4.getRealPath("/");
        System.out.println("项目真实路径为-> " + realPath);
    }
}

```



### 2.9 文件上传和下载

#### 2.9.1 文件上传

文件上传设置前端页面和后台服务器的编写	，前台发送文件，后台接收并保存文件。

> 前端代码

首先我们需要一个表单，并且表单请求方式为**POST**；	其次我们`form`表单的`enctype`必须设置为`multipart/form-data`，意思是设置表单的类型为文件上传表单。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>文件上传</title>
</head>
<body>
    <form method="post" enctype="multipart/form-data" action="up">
        姓名: <input type="text" name="myName"> <br>
        文件: <input type="file" name="myFile"> <br>
        <button>提交</button>
    </form>
</body>
</html>

```



> 后端代码

使用注解**@MultipartConfig**将一个servlet标识为支持文件上传，servlet将`multipart/form-data`的**POST**请求封装成Part，通过Part对上传文件进行操作。

```java
package com.li;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;

/**
 * @author SamLi
 * @version 1.0
 * @data 2023/8/14 - 10:42
 */
@WebServlet("/up")
@MultipartConfig
public class UpLodeServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("文件上传...");
        // 设置编码格式
        request.setCharacterEncoding("utf-8");
        // 获取普通表单项
        String myName = request.getParameter("myName");
        System.out.println("myName: " + myName);
        // 获取part对象
        Part myFile = request.getPart("myFile");
        // 通过part获取上传文件名
        String submittedFileName = myFile.getSubmittedFileName();
        System.out.println("上传文件名: " + submittedFileName);
        // 获取上传文件需要存放的路径
        String filePath = request.getServletContext().getRealPath("/");
        System.out.println("文件存放路径: " + filePath);
        // 将文件上传到指定地方
        myFile.write(filePath + "/" + submittedFileName);
    }
}

```



#### 2.9.2 文件下载

将服务器上的资源copy到本地，我们可以通过两种方式下载：一是通过超链接本身的特性下载，二是通过代码下载。

> 超链接下载

当我们在`HTML`或者`JSP`页面使用a标签是希望可以进行页面跳转，但当超链接遇到浏览器不识别的资源时也会自动下载，当遇到浏览器能够直接显示的资源，浏览器会默认显示出来。当然我们有可以通过**download属性**规定浏览器进行下载。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>文件下载</title>
</head>
<body>
<!--浏览器可以识别的资源 通过download属性规定浏览器进行下载-->
    <a href="Download/文件.txt" download="news.txt">文本文件</a>
    <a href="Download/Jison.jpg">图片文件</a>
<!--浏览器不可以识别的资源-->
    <a href="Download/文件.zip">压缩文件</a>
</body>
</html>

```



> 后台代码下载

1. 需要通过`response.setContentType`方法设置`Content-type`头字段的值，告诉浏览器该资源不可识别；例如`application/x-msdownload`和`application/octet-stream`
2. 需要通过`response.setHeader`方法设置`Content-Disposition`头的值为：`attachment;filename=文件名`
3. 读取下载文件，调用`response.getOutputStream`方法向客户端写入附件内容

```html
<form action="down">
        文件名: <input type="text" name="fileName" placeholder="请输入文件内容">
        <button>下载</button>
</form>
```



```java
package com.li;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author SamLi
 * @version 1.0
 * @data 2023/8/14 - 12:10
 */
@WebServlet("/down")
public class DownloadServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("文件下载...");
        // 设置请求编码格式
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        // 获取下载文件名
        String fileName = request.getParameter("fileName");
        // 参数非空判断
        if (fileName == null || "".equals(fileName.trim())) {
            response.getWriter().write("请输入正确文件名~");
            response.getWriter().close();
            return;
        }
        // 获取下载路径
        String path = request.getServletContext().getRealPath("/Download/");
        // 通过路径获取file对象
        File file = new File(path + fileName);
        // 判断file对象是否存在且是否为一个标准文件
        if (file.exists() && file.isFile()) {
            // 设置响应类型
            response.setContentType("application/x-msdownload");
            // 设置头信息
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            // 得到file文件输入流
            InputStream inputStream = new FileInputStream(file);
            // 得到字节输出流
            ServletOutputStream outputStream = response.getOutputStream();
            // 定义数组和长度
            byte [] bytes = new byte[1024];
            int length = 0;
            // 循环输出
            while ((length = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes,0,length);
            }
            // 关闭流，释放资源
            outputStream.close();
            inputStream.close();
        } else {
            response.getWriter().write("文件不存在，请重试~");
            response.getWriter().close();
        }
    }
}

```

